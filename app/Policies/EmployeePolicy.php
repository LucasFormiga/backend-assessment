<?php

namespace App\Policies;

use App\Employee;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePolicy
{
    use HandlesAuthorization;

    public function delete(User $manager, Employee $employee): bool
    {
        return $manager->id == $employee->manager_id;
    }
}
