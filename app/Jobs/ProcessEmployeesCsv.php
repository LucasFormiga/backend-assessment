<?php

namespace App\Jobs;

use App\Mail\NotifyCsvUpload;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use League\Csv\Reader;

class ProcessEmployeesCsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $filePath;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $reader = Reader::createFromPath(storage_path('app/' . $this->filePath, 'r'));
        $manager = auth()->user();

        if ($reader->fetchOne() != $this->getValidHeader()) {
            return;
        }

        $employees = $reader->fetchAssoc();

        foreach ($employees as $employee) {
            $employee['manager_id'] = $manager->id;

            ProcessEmployee::dispatchNow($employee);
        }

        Mail::to($manager->email)->send(new NotifyCsvUpload);
    }

    private function getValidHeader(): array
    {
        return [
            'name',
            'email',
            'document',
            'city',
            'state',
            'start_date'
        ];
    }
}
