<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApiLoginRequest;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(ApiLoginRequest $request)
    {
        if (! Auth::attempt($request->all())) {
            return response()->json([
                'error' => 'Username or password is incorrect, also this account may not exists.'
            ], 401);
        }

        $user = Auth::user();

        return $user->getApiToken();
    }
}
