<?php

namespace App\Http\Controllers\Api;

use App\Employee;
use App\Events\TriggerEmployeeProcess;
use App\Http\Controllers\Controller;
use App\Http\Requests\CsvUploadRequest;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class EmployeeController extends Controller
{
    public function list()
    {
        $employees = auth()->user()->employees;

        return response()->json($employees, Response::HTTP_OK);
    }

    public function upload(CsvUploadRequest $request)
    {
        $hash = auth()->user()->id . '_' . now()->format('U');
        $filePath = Storage::disk('local')->put("{$hash}", $request->employees);

        event(new TriggerEmployeeProcess($filePath));

        return response()->json(['status' => 'Done!'], Response::HTTP_CREATED);
    }

    public function delete(string $document)
    {
        $employee = Employee::where('document', $document)->firstOrFail();

        $this->authorize('delete', $employee);

        $employee->delete();

        return response()->json([
            'status' => 'Deleted.'
        ], Response::HTTP_NO_CONTENT);
    }
}
