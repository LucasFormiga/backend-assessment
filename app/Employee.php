<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use Uuid;

    /*
    * Defining the guarded attributes,
    * those attributes won't be mass assignable.
    */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $keyType = 'string';

    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }
}
