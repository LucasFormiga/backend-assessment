<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
    Route::namespace('Auth')->prefix('auth')->group(function () {
        Route::post('/login', 'AuthController@login')->name('api.auth.login');
    });

    Route::prefix('employees')->middleware('jwt.auth')->group(function () {
        Route::get('', 'EmployeeController@list')->name('api.employees.list');
        Route::post('/csv', 'EmployeeController@upload')->name('api.employees.upload');
        Route::delete('/{document}', 'EmployeeController@delete')->where('document', '^\d{3}\.\d{3}\.\d{3}\-\d{2}$')->name('api.employees.delete');
    });
});
