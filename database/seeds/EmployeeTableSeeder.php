<?php

use App\Employee;
use App\User;
use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manager = User::where('email', 'dennis.ritchie@convenia.com.br')->first();

        Employee::updateOrCreate([
            'document' => '234.354.645-60'
        ], [
            'name' => 'Lucas Formiga',
            'email' => 'lucas@formiga.com',
            'document' => '234.354.645-60',
            'city' => 'João Pessoa',
            'state' => 'Paraíba',
            'start_date' => '2020-08-01',
            'manager_id' => $manager->id
        ]);
    }
}
